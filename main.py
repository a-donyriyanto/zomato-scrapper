from info_scraper import get_restaurant_info
from review_scraper import get_reviews
from menu_scraper import get_menu
from resto_list import get_resto

def scrape_all_data(url_list):
    """Scrapes all data from the urls passed """

    get_restaurant_info(url_list=url_list, file_name="Restaurants.csv")
    for url in url_list:
        get_reviews(url=url, max_reviews=50, sort="popular", save=True)
        get_menu(url)


if __name__ == '__main__':
    resto_list = get_resto("https://www.zomato.com/id/jakarta/tebet-restoran/kopi", 1)
    for resto in resto_list:
        filename = str(resto[0])
        print(filename)
        filename = filename.strip().replace(' ','_').lower()
        url = resto[4]
        
        get_reviews(url=url, max_reviews=50, sort="popular", save=True, filename=filename)
