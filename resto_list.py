import requests
from bs4 import BeautifulSoup
import pandas as pd
import re
import subprocess
import os
import json 


def get_resto(search_url, max_page):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh;'
                         ' Intel Mac OS X 10_15_4)'
                         ' AppleWebKit/537.36 (KHTML, like Gecko)'
                         ' Chrome/83.0.4103.97 Safari/537.36'}

    # set the iterator pagenumber to automate crawling over given number of pages
    pagenum = 1
    resto_list =[]

    for pagenum in range(1,max_page+1):
        #print(pagenum)

        response = requests.get("{}?page={}".format(search_url, pagenum), headers=headers, timeout=5)
        soup = BeautifulSoup(response.text, 'lxml')
        rlist = soup.find_all('script')
        
        observed = str(rlist[22])
        observed = observed.replace('<script type="text/javascript">'+"\n",'')
        l = 1
        for line in observed.splitlines():
            obs = line
            if l==5:
                break
            l += 1

        obs = obs[0:-1].replace("    zomato.DailyMenuMap.mapData = ","")
        restos = json.loads(obs)
        
        for r in range(1,29):
            try:
                resto_rating = restos[str(r)]["rating"]
                
                if "snippet" in restos[str(r)]:
                    #print(resto_rating)
                    soup = BeautifulSoup(restos[str(r)]["snippet"], "html.parser")
                        
                    #r = soup.find_all("div", {"class": "ui res_name bold"})
                    r = soup.find_all("a", {"data-res-name": True})
                    #resto_name = re.sub(r"\n", "", r[0]["data-res-name"]) #.getText())
                    resto_name = r[0]["data-res-name"]
                    #print(resto_name.strip())
                    
                    r = soup.find_all("div", {"class": "search-result-address"})
                    resto_address = r[0].getText()
                    #print(resto_address.strip())
                    
                    r = soup.find_all("a", {"data-phone-no-str": True})
                    resto_phone = r[0]["data-phone-no-str"].replace(" ","")
                    #print(resto_phone)
                    
                    
                    r = soup.find_all("a", {"class": "result-title"})
                    resto_url = r[0]["href"]
                    #print(resto_url)
                    '''    
                    resto_url = ''
                    '''
                    #print('==================')
                    resto_list.append((
                        resto_name,
                        resto_address,
                        resto_phone,
                        resto_rating,
                        resto_url
                    ))
            except:
                break

    columns = ['Name', 'Address', 'Phone', 'Rating', 'Url']
    resto_df = pd.DataFrame(resto_list, columns=columns)
    if not os.path.exists("RestoList"):
        os.makedirs("RestoList")
    resto_df.to_csv(f"RestoList/list.csv", index=False)
    return resto_list

if __name__ == "__main__":
    get_resto("https://www.zomato.com/id/jakarta/tebet-restoran/kopi", 1)